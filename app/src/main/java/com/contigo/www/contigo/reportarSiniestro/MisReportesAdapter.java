package com.contigo.www.contigo.reportarSiniestro;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.Reportes;
import com.contigo.www.contigo.reporte.ReporteActivity;

import java.util.List;

/**
 * Created by daniel on 31/03/17.
 */


public class MisReportesAdapter  extends RecyclerView.Adapter<MisReportesAdapter.ViewHolder> {
    private final Context context;
    private List<Reportes> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class  ViewHolder extends RecyclerView.ViewHolder {
        de.hdodenhof.circleimageview.CircleImageView tv_img;
        public TextView titulo;
        public TextView subtitulo;
        public Button button;
        ViewHolder(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            subtitulo = (TextView) itemView.findViewById(R.id.subtitulo);
            button = (Button) itemView.findViewById(R.id.button);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MisReportesAdapter (List<Reportes> myDataset,Context conte) {
        mDataset = myDataset;
        this.context = conte;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mis_reportes, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.titulo.setText("Reporte "+mDataset.get(position).getFecha());
        holder.subtitulo.setText("En proceso de aprobación");

        holder.button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ReporteActivity.class);
                intent.putExtra("fecha",mDataset.get(position).getFecha());
                context.startActivity(intent);

            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}