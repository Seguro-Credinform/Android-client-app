package com.contigo.www.contigo.nuestrasOfertasLogueado;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.Ofertas;

import java.util.Collections;
import java.util.List;

/**
 * Created by daniel on 12/05/17.
 */

public class SubActivityAdapte extends RecyclerView.Adapter<SubActivityAdapte.SubActivityViewHolder> {

    private final LayoutInflater inflater;
    List<Ofertas> subActivityData = Collections.EMPTY_LIST;

    public SubActivityAdapte(Context context, List<Ofertas> subActivityData) {
        inflater = LayoutInflater.from(context);
        this.subActivityData = subActivityData;
    }


    @Override
    public SubActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_nuestras_ofertas, parent, false);
        SubActivityViewHolder subActivityViewHolder = new SubActivityViewHolder(view);
        return subActivityViewHolder;
    }

    @Override
    public void onBindViewHolder(SubActivityViewHolder holder, int position) {
        Ofertas currentCard = subActivityData.get(position);
        holder.title.setText(currentCard.getTitulo());

    }

    @Override
    public int getItemCount() {
        return subActivityData.size();
    }

    class SubActivityViewHolder extends RecyclerView.ViewHolder {

        TextView title;

        public SubActivityViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.card_text);
        }
    }

}