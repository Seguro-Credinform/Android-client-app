package com.contigo.www.contigo.planDePagos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.Pagos;

import java.util.ArrayList;
import java.util.List;


public class VencidosFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayout vacio;

    public VencidosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_vencidos, container, false);

        TextView textView4;
        textView4 = (TextView) v.findViewById(R.id.textView4);
        //textView3.setText(" inferior derecha,");
        SpannableString text4 = new SpannableString("Aún no has reportado ningun siniestro. Para reportar haz click en el icono azul de chat en la parte inferior derecha, de esta forma te contactarás con un Inspector Móvil que inicie el proceso de reporte.");

        text4.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 57, 79, 0);

        textView4.setText(text4, TextView.BufferType.SPANNABLE);
        // yourtextview.setText(Html.fromHtml(text));


        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        vacio = (LinearLayout) v.findViewById(R.id.vacio);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        List<Pagos> repo = new ArrayList<Pagos>();
        repo.add(new Pagos(3, "Seguro Familiar","07/03/2017","vencido"));
        repo.add(new Pagos(4, "Seguro de Inmueble","27/02/2017","vencido"));



        if (repo.size() > 0) {
            vacio.setVisibility(View.GONE);
        }
        mAdapter = new MisPagosVigentesAdapter(repo, getContext());
        mRecyclerView.setAdapter(mAdapter);

        return v;

    }
}