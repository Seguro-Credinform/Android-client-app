package com.contigo.www.contigo.informacionDeServicio;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.Servicios;

import java.util.Collections;
import java.util.List;

/**
 * Created by daniel on 12/05/17.
 */

public class RecyclerInformacionDeServiciosAdapte extends RecyclerView.Adapter<RecyclerInformacionDeServiciosAdapte.SubActivityViewHolder> {

    private final LayoutInflater inflater;
    List<Servicios> subActivityData = Collections.EMPTY_LIST;
    private Context mContext;
    private AlertDialog.Builder builder;

    public RecyclerInformacionDeServiciosAdapte(Context context, List<Servicios> subActivityData) {
        inflater = LayoutInflater.from(context);
        this.subActivityData = subActivityData;
        this.mContext = context;
        builder = new AlertDialog.Builder(this.mContext);
    }


    @Override
    public SubActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_informacion_de_servicios, parent, false);
        SubActivityViewHolder subActivityViewHolder = new SubActivityViewHolder(view);
        return subActivityViewHolder;
    }


    @Override
    public void onBindViewHolder(SubActivityViewHolder holder, int position) {
        Servicios currentCard = subActivityData.get(position);
        holder.title.setText(currentCard.getTitulo());
       // int resID = this.mContext.getResources().getIdentifier(currentCard.getImagen().get(1).getPequena() , "drawable", this.mContext.getPackageName());
        Glide.with(this.mContext).load(currentCard.getImagen().get(0).getPequena()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(holder.image);
        //int resID = this.mContext.getResources().getIdentifier("informacionservicios1" , "drawable", this.mContext.getPackageName());
        //holder.image.setImageResource(resID);

    }

    @Override
    public int getItemCount() {
        return subActivityData.size();
    }

    class SubActivityViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        TextView title;
        ImageView image;


        public SubActivityViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            title = (TextView) itemView.findViewById(R.id.card_text);
            image = (ImageView) itemView.findViewById(R.id.imageView13);
        }


        @Override
        public void onClick(View view) {
          //  view.createSimpleDialog(subActivityData.get(getPosition()).getTitulo(), subActivityData.get(getPosition()).getDescripcion());


          /*  builder.setTitle(subActivityData.get(getPosition()).getTitulo())
                    .setMessage(subActivityData.get(getPosition()).getDescripcion())
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });

            AlertDialog dialog = builder.create();*/
           // FragmentManager fragmentManager = view.getContext().getSupportFragmentManager();

            AlertDialog dia = createSimpleDialog(subActivityData.get(getPosition()).getTitulo(), subActivityData.get(getPosition()).getDescripcion());
            dia.show();
            Log.d( "onClick ",  " posicion" + subActivityData.get(getPosition()).getTitulo());
        }
    }

    public AlertDialog createSimpleDialog(String titulo, String descripcion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);

        builder.setTitle(titulo)
                .setMessage(descripcion)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        return builder.create();
    }






}