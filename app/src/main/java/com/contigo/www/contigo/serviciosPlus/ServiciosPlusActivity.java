package com.contigo.www.contigo.serviciosPlus;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.reportarSiniestroConectando.ReportarSiniestroConectandoActivity;

public class ServiciosPlusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios_plus);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Servicios Plus");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        setSupportActionBar(toolbar);
        TextView textView12;
        textView12 = (TextView) findViewById(R.id.textView12);
        //textView3.setText(" inferior derecha,");
        SpannableString text12 = new SpannableString("Lo sentimos, usted aún no ha solicitado una tarjeta SERVIPLUS para acceder a nuestos servicios plus.");

        text12.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorPrimary)), 52, 61, 0);

        textView12.setText(text12, TextView.BufferType.SPANNABLE);

        TextView textView13;
        textView13 = (TextView) findViewById(R.id.textView13);
        //textView3.setText(" inferior derecha,");
        SpannableString text13 = new SpannableString("Para mayor información puede consultar a un operador online presionando el icono azul chat");

        text13.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorPrimary)), 72, 85, 0);

        textView13.setText(text13, TextView.BufferType.SPANNABLE);

        TextView textView14;
        textView14 = (TextView) findViewById(R.id.textView14);
        SpannableString text14 = new SpannableString("en la parte inferior derecha, o visite nuestra pagina web informativa en el siguiente link.");

        text14.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorPrimary)), 86, 90, 0);

        textView14.setText(text14, TextView.BufferType.SPANNABLE);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiciosPlusActivity.this, ReportarSiniestroConectandoActivity.class);
                intent.putExtra("titulo", "Consulta Online" );
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
