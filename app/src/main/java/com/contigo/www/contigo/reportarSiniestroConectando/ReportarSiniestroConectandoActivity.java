package com.contigo.www.contigo.reportarSiniestroConectando;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.chatReportarSiniestro.ChatReportarSiniestroActivity;

public class ReportarSiniestroConectandoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportar_siniestro_conectando);
        String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= "Reportar Siniestro";
            } else {
                newString= extras.getString("titulo");
            }
        } else {
            newString= (String) savedInstanceState.getSerializable("titulo");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(newString);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        setSupportActionBar(toolbar);
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Intent intent = new Intent(ReportarSiniestroConectandoActivity.this, ChatReportarSiniestroActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, 2000);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}
