package com.contigo.www.contigo.reportarSiniestro;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.reportarSiniestroConectando.ReportarSiniestroConectandoActivity;

public class ReportarSiniestroActivity extends AppCompatActivity {

    static final int NUM_ITEMS = 2;
    ViewPager mPager;
    TabLayout tabs;
    private String[] tabTitles = new String[]{"Mis Reportes", "Mis Chats"};
    SlidePagerAdapter mPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportar_siniestro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Reportar Siniestro");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        setSupportActionBar(toolbar);
        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.addTab(tabs.newTab().setText("Mis Reportes"));
        tabs.addTab(tabs.newTab().setText("Mis Chats"));

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new SlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReportarSiniestroActivity.this, ReportarSiniestroConectandoActivity.class);
                startActivity(intent);
            }
        });
    }

    /* PagerAdapter class */
    public class SlidePagerAdapter extends FragmentPagerAdapter {
        public SlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // overriding getPageTitle()
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }


        @Override
        public Fragment getItem(int position) {
			/*
			 * IMPORTANT: This is the point. We create a RootFragment acting as
			 * a container for other fragments
			 */
            if (position == 0) {
                mPager.setCurrentItem(position);
                tabs.setupWithViewPager(mPager);
                return new ReportesFragment();
            }else {
                mPager.setCurrentItem(position);
                tabs.setupWithViewPager(mPager);
                return new MisChatsFragment();
            }
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
