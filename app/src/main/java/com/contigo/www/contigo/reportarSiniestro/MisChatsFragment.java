package com.contigo.www.contigo.reportarSiniestro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.contigo.www.contigo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MisChatsFragment extends Fragment {


    public MisChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mis_chats, container, false);
       // View v = inflater.inflate(R.layout.fragment_a, container, false);
        TextView textView3;
        textView3 = (TextView) v.findViewById(R.id.textView3);
        //textView3.setText(" inferior derecha,");
        SpannableString text3 = new SpannableString("Aún no has reportado ningun siniestro. Para reportar haz click en el icono azul de chat en la parte inferior derecha, de esta forma te contactarás con un Inspector Móvil que inicie el proceso de reporte.");

        text3.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(),R.color.colorPrimary)), 57, 79, 0);

        textView3.setText(text3, TextView.BufferType.SPANNABLE);
        // yourtextview.setText(Html.fromHtml(text));
        return v;
    }

}
