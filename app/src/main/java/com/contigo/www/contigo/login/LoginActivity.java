package com.contigo.www.contigo.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.nuestrasOfertasLogueado.NuestrasOfertasLogueadoActivity;

public class LoginActivity extends AppCompatActivity {
    EditText editTextUsuario;
    EditText editTextClave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editTextUsuario = (EditText)findViewById(R.id.editTextUsuario);
        editTextClave = (EditText)findViewById(R.id.editTextClave);
       editTextUsuario.setPadding(70,15,15,15);
        editTextClave.setPadding(70,15,15,15);
        Button botonIngresar = (Button) findViewById(R.id.botonIngresar);
        botonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, NuestrasOfertasLogueadoActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

}
