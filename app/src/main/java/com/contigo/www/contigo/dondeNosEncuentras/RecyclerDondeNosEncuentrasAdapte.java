package com.contigo.www.contigo.dondeNosEncuentras;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.DondeNosEncuentras;

import java.util.Collections;
import java.util.List;

/**
 * Created by daniel on 12/05/17.
 */

public class RecyclerDondeNosEncuentrasAdapte extends RecyclerView.Adapter<RecyclerDondeNosEncuentrasAdapte.SubActivityViewHolder> {

    private final LayoutInflater inflater;
    List<DondeNosEncuentras> subActivityData = Collections.EMPTY_LIST;

    public RecyclerDondeNosEncuentrasAdapte(Context context, List<DondeNosEncuentras> subActivityData) {
        inflater = LayoutInflater.from(context);
        this.subActivityData = subActivityData;
    }


    @Override
    public SubActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_donde_nos_encontramos, parent, false);
        SubActivityViewHolder subActivityViewHolder = new SubActivityViewHolder(view);
        return subActivityViewHolder;
    }

    @Override
    public void onBindViewHolder(SubActivityViewHolder holder, int position) {
        DondeNosEncuentras currentCard = subActivityData.get(position);
        holder.title.setText(currentCard.getNombre());
        holder.subTitulo.setText(currentCard.getDireccion());
        holder.telefono.setText(currentCard.getTelefono());

    }

    @Override
    public int getItemCount() {
        return subActivityData.size();
    }

    class SubActivityViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView subTitulo;
        TextView telefono;

        public SubActivityViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.card_text);
            subTitulo = (TextView) itemView.findViewById(R.id.card_text1);
            telefono = (TextView) itemView.findViewById(R.id.card_telefono);
        }
    }

}