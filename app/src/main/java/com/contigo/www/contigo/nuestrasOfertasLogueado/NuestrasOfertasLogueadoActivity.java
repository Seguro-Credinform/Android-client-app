package com.contigo.www.contigo.nuestrasOfertasLogueado;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.calculadoraDePoliza.BienvenidoCalculadoraDePolizaActivity;
import com.contigo.www.contigo.dondeNosEncuentras.DondeNosEncuentrasActivity;
import com.contigo.www.contigo.emergencias.EmergenciasTelefonosActivity;
import com.contigo.www.contigo.entidades.Ofertas;
import com.contigo.www.contigo.informacionDeServicio.InformacionDeServicioActivity;
import com.contigo.www.contigo.nuestrasOfertas.NuestasOfertasActivity;
import com.contigo.www.contigo.perfil.PerfilActivity;
import com.contigo.www.contigo.planDePagos.PlanDePagosActivity;
import com.contigo.www.contigo.reportarSiniestro.ReportarSiniestroActivity;
import com.contigo.www.contigo.reportarSiniestroConectando.ReportarSiniestroConectandoActivity;
import com.contigo.www.contigo.serviciosPlus.ServiciosPlusActivity;

import java.util.ArrayList;
import java.util.List;

public class NuestrasOfertasLogueadoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    static final int NUM_ITEMS = 2;
    private DrawerLayout mDrawerLayout;
    private RecyclerView mRecyclerView;
    private SubActivityAdapte mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuestras_ofertas_logueado);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Nuestras Ofertas");
        toolbar.setNavigationIcon(R.drawable.ic_menu_grey600_24dp);
        setSupportActionBar(toolbar);



        //RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.hrlist_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new SubActivityAdapte(this, getData());
        mRecyclerView.setAdapter(mAdapter);

        //Layout manager for the Recycler View
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(this);

        TextView textView3;
        textView3 = (TextView) findViewById(R.id.textView3);
        //textView3.setText(" inferior derecha,");
        SpannableString text3 = new SpannableString("Hola Usuario");



        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        text3.setSpan(bss, 5, 12, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        text3.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorPrimary)), 5, 12, 0);

        textView3.setText(text3, TextView.BufferType.SPANNABLE);



        TextView textView10;
        textView10 = (TextView) findViewById(R.id.textView10);
        //textView3.setText(" inferior derecha,");
        SpannableString text10 = new SpannableString("Tu seguro de inmueble esta a punto de vencer");

        final StyleSpan bsss = new StyleSpan(android.graphics.Typeface.BOLD);
        text10.setSpan(bsss, 38, 44, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        textView10.setText(text10, TextView.BufferType.SPANNABLE);



        View header = mNavigationView.getHeaderView(0);
        TextView editarPerfil = (TextView) header.findViewById(R.id.editarPerfil);
        editarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, PerfilActivity.class);
                startActivity(intent);
            }
        });



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, ReportarSiniestroConectandoActivity.class);
                intent.putExtra("titulo", "Consulta Online" );
                startActivity(intent);
            }
        });


        // ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

    }

    public static List<Ofertas> getData() {

        List<Ofertas> data = new ArrayList<Ofertas>();
        data.add(new Ofertas("Seguro Familiar","Fidelidad de Empleados"));
        data.add(new Ofertas("Seguro Familiar","Fidelidad de Empleados"));
        data.add(new Ofertas("Seguro Familiar","Fidelidad de Empleados"));
        data.add(new Ofertas("Seguro Familiar","Fidelidad de Empleados"));

        /*if (data.size() > 0) {
            vacio.setVisibility(View.GONE);
        }*/



        return data;
    }






    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
       // mDrawerLayout.closeDrawers();

        switch (item.getItemId()) {

            case R.id.nav_reportar_siniestro: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, ReportarSiniestroActivity.class);
                startActivity(intent);
                return true;

            }case R.id.nav_emergencias: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, EmergenciasTelefonosActivity.class);
                startActivity(intent);
                return true;

            }case R.id.nav_servicios_plus:{
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, ServiciosPlusActivity.class);
                startActivity(intent);
                return true;

            }
            case R.id.nav_plan_de_pagos: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, PlanDePagosActivity.class);
                startActivity(intent);
                return true;

            }
            case R.id.nav_calcular_poliza: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, BienvenidoCalculadoraDePolizaActivity.class);
                startActivity(intent);
                return true;
            }
            case  R.id.nav_informacion_de_servicios: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, InformacionDeServicioActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.nav_consultas_online: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, ReportarSiniestroConectandoActivity.class);
                intent.putExtra("titulo", "Consulta Online" );
                startActivity(intent);
                return true;
            }
            case R.id.nav_ofertas: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, NuestasOfertasActivity.class);
                startActivity(intent);
                return true;
            }
            case R.id.nav_donde: {
                Intent intent = new Intent(NuestrasOfertasLogueadoActivity.this, DondeNosEncuentrasActivity.class);
                startActivity(intent);
                return true;
            }




        }


        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                Log.i("dddf","dsfdsfds");
                mDrawerLayout.openDrawer(Gravity.LEFT);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}
