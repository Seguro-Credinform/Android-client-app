package com.contigo.www.contigo.bienvenido;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.calculadoraDePoliza.BienvenidoCalculadoraDePolizaActivity;
import com.contigo.www.contigo.dondeNosEncuentras.DondeNosEncuentrasActivity;
import com.contigo.www.contigo.informacionDeServicio.InformacionDeServicioActivity;
import com.contigo.www.contigo.login.LoginActivity;
import com.contigo.www.contigo.nuestrasOfertas.NuestasOfertasActivity;
import com.contigo.www.contigo.reportarSiniestroConectando.ReportarSiniestroConectandoActivity;

public class BienvenidoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenido);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Bienvenido");
        toolbar.setNavigationIcon(R.drawable.ic_menu_grey600_24dp);
        setSupportActionBar(toolbar);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(this);


        TextView textView3;
        textView3 = (TextView) findViewById(R.id.textView3);
        //textView3.setText(" inferior derecha,");
        SpannableString text3 = new SpannableString("ContigoAPP es la aplicación que te ayudará a mantenerte informado acerca de nuestros servicios y mantenerte en contacto frecuente con nuestros operadores.");

        text3.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this,R.color.colorPrimary)), 0, 10, 0);

        textView3.setText(text3, TextView.BufferType.SPANNABLE);

        // ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        Button aseguradoButtom = (Button) findViewById(R.id.aseguradoButtom);
        aseguradoButtom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BienvenidoActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.bienvenido, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calcular_poliza) {
            Intent intent = new Intent(BienvenidoActivity.this, BienvenidoCalculadoraDePolizaActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_informacion_de_servicios) {
            Intent intent = new Intent(BienvenidoActivity.this, InformacionDeServicioActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_consultas_online) {
            Intent intent = new Intent(BienvenidoActivity.this, ReportarSiniestroConectandoActivity.class);
            intent.putExtra("titulo", "Consulta Online" );
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_ofertas) {
            Intent intent = new Intent(BienvenidoActivity.this, NuestasOfertasActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_donde) {
            Intent intent = new Intent(BienvenidoActivity.this, DondeNosEncuentrasActivity.class);
            startActivity(intent);
            return true;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
