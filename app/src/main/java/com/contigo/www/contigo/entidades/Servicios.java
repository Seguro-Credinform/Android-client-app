package com.contigo.www.contigo.entidades;

import java.util.List;

/**
 * Created by daniel on 12/05/17.
 */

public class Servicios {
    private Integer idInformacion;
    private List<Imagenes> imagen;
    private String titulo;
    private String descripcion;
    private Integer status;

    public void Servicios() {

    }
    public void Servicios(Integer idInformacion, String titulo,  String descripcion, List<Imagenes> imagen, Integer status) {
        this.idInformacion = idInformacion;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.status = status;
    }




    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIdInformacion() {
        return idInformacion;
    }

    public void setIdInformacion(Integer idInformacion) {
        this.idInformacion = idInformacion;
    }

    public List<Imagenes> getImagen() {
        return imagen;
    }

    public void setImagen(List<Imagenes> imagen) {
        this.imagen = imagen;
    }
}
