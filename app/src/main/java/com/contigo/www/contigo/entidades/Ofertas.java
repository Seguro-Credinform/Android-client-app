package com.contigo.www.contigo.entidades;

/**
 * Created by daniel on 12/05/17.
 */

public class Ofertas {
    private String Imagen;
    private String Titulo;
    private String Subtitulo;

    public Ofertas(String imagen, String titulo) {
        Imagen = imagen;
        Titulo = titulo;
    }

    public Ofertas(String imagen, String titulo, String subtitulo) {
        Imagen = imagen;
        Titulo = titulo;
        Subtitulo = subtitulo;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }

    public void setSubtitulo(String subtitulo) {
        Subtitulo = subtitulo;
    }

    public String getSubtitulo() {
        return Subtitulo;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }
}
