package com.contigo.www.contigo.nuestrasOfertas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.Ofertas;
import com.contigo.www.contigo.reportarSiniestroConectando.ReportarSiniestroConectandoActivity;

import java.util.ArrayList;
import java.util.List;

public class NuestasOfertasActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerNuestrasOfertasAdapte mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuestas_ofertas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Nuestras Ofertas");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        setSupportActionBar(toolbar);


        //RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.hhrlist_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new RecyclerNuestrasOfertasAdapte(this, getData());
        mRecyclerView.setAdapter(mAdapter);

        //Layout manager for the Recycler View
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NuestasOfertasActivity.this, ReportarSiniestroConectandoActivity.class);
                intent.putExtra("titulo", "Consulta Online" );
                startActivity(intent);
            }
        });
    }

    public static List<Ofertas> getData() {

        List<Ofertas> data = new ArrayList<Ofertas>();
        data.add(new Ofertas("Seguro Familiar","Seguro Familiar","¿Cuáles son los casos en que no aplica la cobertura de un SOAT?"));
        data.add(new Ofertas("Seguro Familiar","Seguro Familiar","¿En qué circunstancias la compañía Aseguradora pagará las indemnizaciones pero tendrá el derecho de cobrar al responsable del accidente?"));
        data.add(new Ofertas("Seguro Familiar","Seguro Familiar","¿Cuantos días tiene la Compañia Aseguradora para cumplir con el pago de las indemnizaciones?"));
        data.add(new Ofertas("Seguro Familiar","Seguro Familiar","¿Cuál es el monto de capital que se puede cobrar como víctimas de un accdente?"));


        /*if (data.size() > 0) {
            vacio.setVisibility(View.GONE);
        }*/



        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}
