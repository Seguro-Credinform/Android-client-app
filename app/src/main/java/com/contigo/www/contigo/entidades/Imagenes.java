package com.contigo.www.contigo.entidades;

/**
 * Created by daniel on 05/08/17.
 */

public class Imagenes {
    private String pequena;
    private String mediana;
    private String grande;

    public  Imagenes(){

    }
    public  Imagenes(String pequena, String mediana, String grande){
        this.pequena = pequena;
        this.mediana = mediana;
        this.grande = grande;
    }

    public String getPequena() {
        return pequena;
    }

    public void setPequena(String pequena) {
        this.pequena = pequena;
    }

    public String getMediana() {
        return mediana;
    }

    public void setMediana(String mediana) {
        this.mediana = mediana;
    }

    public String getGrande() {
        return grande;
    }

    public void setGrande(String grande) {
        this.grande = grande;
    }
}
