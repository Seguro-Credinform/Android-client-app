package com.contigo.www.contigo.informacionDeServicio;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Response;
import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.Servicios;
import com.contigo.www.contigo.services.Peticiones;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class InformacionDeServicioActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerInformacionDeServiciosAdapte mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_de_servicio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Información de Servicios");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        setSupportActionBar(toolbar);


        //RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.hhrlist_recycler_view);
        mRecyclerView.setHasFixedSize(true);


        Peticiones peticiones = new Peticiones();

        peticiones.obtener("informaciones/", this, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
              //  Log.i("Response is: ", response);
                Type listType = new TypeToken<ArrayList<Servicios>>(){}.getType();
                List<Servicios> lista = new Gson().fromJson(response, listType);
                Log.i("Response is: ", String.valueOf(lista.size()));
                mAdapter = new RecyclerInformacionDeServiciosAdapte(InformacionDeServicioActivity.this, lista);
                mRecyclerView.setAdapter(mAdapter);
                mLayoutManager = new LinearLayoutManager(InformacionDeServicioActivity.this, LinearLayoutManager.VERTICAL, false);
                mRecyclerView.setLayoutManager(mLayoutManager);

            }
        });
       // mAdapter = new RecyclerInformacionDeServiciosAdapte(this, getData());
      //  mRecyclerView.setAdapter(mAdapter);

        //Layout manager for the Recycler View




    }

    public static List<Servicios> getData() {

        List<Servicios> data = new ArrayList<Servicios>();

      /*  data.add(new Servicios("informacionservicios1","¿Cuáles son los casos en que no aplica la cobertura de un SOAT?"));
        data.add(new Servicios("informacionservicios2","¿En qué circunstancias la compañía Aseguradora pagará las indemnizaciones pero tendrá el derecho de cobrar al responsable del accidente?"));
        data.add(new Servicios("informacionservicios3","¿Cuantos días tiene la Compañia Aseguradora para cumplir con el pago de las indemnizaciones?"));
        data.add(new Servicios("informacionservicios4","¿Cuál es el monto de capital que se puede cobrar como víctimas de un accdente?"));
*/

        /*if (data.size() > 0) {
            vacio.setVisibility(View.GONE);
        }*/



        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}
