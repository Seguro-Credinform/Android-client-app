package com.contigo.www.contigo.chatReportarSiniestro;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

import com.contigo.www.contigo.R;

public class ChatReportarSiniestroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_reportar_siniestro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      /*  toolbar.setTitle("Nombre Apellido");
        toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
        toolbar.setSubtitle("Inspector Móvil");
        toolbar.setSubtitleTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        toolbar.setLogo(R.drawable.useravatardefault);
*/
        setSupportActionBar(toolbar);

        ImageView icon_atras = (ImageView) findViewById(R.id.icon_atras);
        icon_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_chat_reportar_siniestro, menu);

        return super.onCreateOptionsMenu(menu);
    }


}
