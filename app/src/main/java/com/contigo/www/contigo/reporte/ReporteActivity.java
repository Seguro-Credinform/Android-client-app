package com.contigo.www.contigo.reporte;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.contigo.www.contigo.R;

public class ReporteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView text_toolbar_title = (TextView) findViewById(R.id.text_toolbar_title);

        Bundle action = getIntent().getExtras();
            String fecha;
        if(action == null) {
            fecha = "00/00/0000";
        }else{
            fecha = action.getString("fecha");
        }
        text_toolbar_title.setText("Reporte "+fecha);

        setSupportActionBar(toolbar);


        ImageView icon_atras = (ImageView) findViewById(R.id.icon_atras);
        icon_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

}
