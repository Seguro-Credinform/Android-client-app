package com.contigo.www.contigo.entidades;

/**
 * Created by daniel on 05/08/17.
 */

public class TiposMensajes {
    private Integer idTipoMensaje;
    private String nombre;
    private  Imagenes imagenes;

    public TiposMensajes(){

    }
    public TiposMensajes(Integer idTipoMensaje, String nombre, Imagenes imagenes){
        this.idTipoMensaje = idTipoMensaje;
        this.nombre = nombre;
        this.imagenes = imagenes;
    }

    public Integer getIdTipoMensaje() {
        return idTipoMensaje;
    }

    public void setIdTipoMensaje(Integer idTipoMensaje) {
        this.idTipoMensaje = idTipoMensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Imagenes getImagenes() {
        return imagenes;
    }

    public void setImagenes(Imagenes imagenes) {
        this.imagenes = imagenes;
    }
}
