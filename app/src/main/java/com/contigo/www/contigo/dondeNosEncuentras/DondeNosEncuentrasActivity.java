package com.contigo.www.contigo.dondeNosEncuentras;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.entidades.DondeNosEncuentras;

import java.util.ArrayList;
import java.util.List;

public class DondeNosEncuentrasActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerDondeNosEncuentrasAdapte mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donde_nos_encuentras);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Donde nos Encuentras");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_grey600_24dp);
        setSupportActionBar(toolbar);

        //RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.hhrlist_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new RecyclerDondeNosEncuentrasAdapte(this, getData());
        mRecyclerView.setAdapter(mAdapter);

        //Layout manager for the Recycler View
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }


    public static List<DondeNosEncuentras> getData() {

        List<DondeNosEncuentras> data = new ArrayList<DondeNosEncuentras>();
        data.add(new DondeNosEncuentras("La Paz(Of. Principal)","Calacoco-calle Julio Patiño Nro. 550 esquina calle 12","(591-1) 2775550"));
        data.add(new DondeNosEncuentras("La Paz(Sucursal 1)","Calle Capitan Racel Nro. 2328 entre Belisario Salinas y Rosendo Gutierrez","(591-1) 2775550"));
        data.add(new DondeNosEncuentras("La Paz(Of. Principal)","Calacoco-calle Julio Patiño Nro. 550 esquina calle 12","(591-1) 2775550"));
        data.add(new DondeNosEncuentras("La Paz(Sucursal 1)","Calle Capitan Racel Nro. 2328 entre Belisario Salinas y Rosendo Gutierrez","(591-1) 2775550"));

        /*if (data.size() > 0) {
            vacio.setVisibility(View.GONE);
        }*/



        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {



            case android.R.id.home:
                finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}
