package com.contigo.www.contigo.entidades;

/**
 * Created by daniel on 07/04/17.
 */

public class Reportes {
    private Integer id_reporte;
    private String fecha;
    private Integer estatus;



    public Reportes(Integer id_reporte, String fecha, Integer estatus){
        this.id_reporte = id_reporte;
        this.fecha = fecha;
        this.estatus = estatus;

    }

    public Reportes(){

    }
    public Integer getId_reporte() {
        return id_reporte;
    }

    public void setId_reporte(Integer id_reporte) {
        this.id_reporte = id_reporte;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }
}
