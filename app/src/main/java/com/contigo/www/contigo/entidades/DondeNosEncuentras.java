package com.contigo.www.contigo.entidades;

/**
 * Created by daniel on 24/05/17.
 */

public class DondeNosEncuentras {
    private String nombre;
    private String direccion;
    private String telefono;

    public DondeNosEncuentras(String nombre, String direccion, String telefono){
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
