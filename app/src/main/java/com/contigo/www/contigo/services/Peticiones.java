package com.contigo.www.contigo.services;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by daniel on 27/07/17.
 */

public class Peticiones<T> {
    private String urlBase = "http://192.34.63.79:8445/v1/";

    public Peticiones() {
    }

    public void obtener(String url, Context contexto, Response.Listener<String> responseListener){
        RequestQueue queue = Volley.newRequestQueue(contexto);
        queue.add( new StringRequest(Request.Method.GET, urlBase+url, responseListener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("malo","error en la peticion");
            }
        }));
    }


}
