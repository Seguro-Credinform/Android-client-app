package com.contigo.www.contigo.entidades;

/**
 * Created by daniel on 20/04/17.
 */

public class Pagos {
    private Integer idPago;
    private String nombre;
    private String vence;
    private String estatus;

    public Pagos(){

    }

    public Pagos(Integer idPago, String nombre, String vence, String estatus){
        this.idPago = idPago;
        this.nombre = nombre;
        this.vence = vence;
        this.estatus = estatus;
    }

    public Integer getIdPago() {
        return idPago;
    }

    public void setIdPago(Integer idPago) {
        this.idPago = idPago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVence() {
        return vence;
    }

    public void setVence(String vence) {
        this.vence = vence;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
}
