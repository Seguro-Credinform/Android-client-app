package com.contigo.www.contigo.planDePagos;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.contigo.www.contigo.R;
import com.contigo.www.contigo.detalleSeguro.DetalleSeguroActivity;
import com.contigo.www.contigo.entidades.Pagos;

import java.util.List;

/**
 * Created by daniel on 31/03/17.
 */


public class MisPagosVigentesAdapter extends RecyclerView.Adapter<MisPagosVigentesAdapter.ViewHolder> {
    private final Context context;
    private List<Pagos> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder

    public static class  ViewHolder extends RecyclerView.ViewHolder {
        de.hdodenhof.circleimageview.CircleImageView tv_img;
        public TextView titulo;
        public RelativeLayout contenedor;
        public TextView subtitulo;
        public ImageView imagen;
        ViewHolder(View itemView) {
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            contenedor = (RelativeLayout) itemView.findViewById(R.id.contenedor);
            subtitulo = (TextView) itemView.findViewById(R.id.subtitulo);
            imagen = (ImageView) itemView.findViewById(R.id.imageView12);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MisPagosVigentesAdapter(List<Pagos> myDataset, Context conte) {
        mDataset = myDataset;
        context = conte;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mis_planes_vigentes, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetalleSeguroActivity.class);
                context.startActivity(intent);

            }
        });
        holder.titulo.setText("Reporte "+mDataset.get(position).getNombre());
        holder.subtitulo.setText("vence: "+mDataset.get(position).getVence());
       if((mDataset.get(position).getEstatus().equals("vencido"))||(mDataset.get(position).getEstatus().equals("bien"))){
           holder.imagen.setVisibility(View.GONE);
       }else if(mDataset.get(position).getEstatus().equals("cerca_vencer")){
           holder.imagen.setImageTintList(ColorStateList.valueOf(Color.YELLOW));
           holder.subtitulo.setTextColor(ColorStateList.valueOf(Color.YELLOW));
       }else if(mDataset.get(position).getEstatus().equals("por_vencer")){
           holder.imagen.setImageTintList(ColorStateList.valueOf(Color.RED));
           holder.subtitulo.setTextColor(ColorStateList.valueOf(Color.RED));
       }





    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}