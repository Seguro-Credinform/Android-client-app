package com.contigo.www.contigo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Response;
import com.contigo.www.contigo.bienvenido.BienvenidoActivity;
import com.contigo.www.contigo.entidades.TiposMensajes;
import com.contigo.www.contigo.services.Peticiones;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button botonEmpezar = (Button) findViewById(R.id.botonEmpezar);
        botonEmpezar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BienvenidoActivity.class);
                startActivity(intent);
                finish();

            }
        });


         String url = "informaciones/";
        Peticiones peticiones = new Peticiones();

        peticiones.obtener(url, this, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Type listType = new TypeToken<ArrayList<TiposMensajes>>(){}.getType();
                List<TiposMensajes> lista = new Gson().fromJson(response, listType);
                Log.i("Response is: ", String.valueOf(lista.size()));

            }
        });


   }
}